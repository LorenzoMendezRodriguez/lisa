from ConexionServidor import *



#Recibe solicitudes hasta que su tabla de routeo este vacia
def main():
	conexion = ConexionServidor()
	print('Levantandome en:'+ conexion.tcp_ip +' puerto: ' + str(conexion.puerto))
	t = 1
	while(t):
		print('Esperando Solicitud....\n')
		paquete = conexion.esperar_solicitud()
		solicitud = paquete[Raw].load
		mensaje = conexion.atender_solicitud(paquete)
		if(mensaje != -1 and mensaje != 1):
			puerto = paquete[TCP].sport
			ip = paquete[IP].src
			time.sleep(3)	
			conexion.responder_solicitud(mensaje, ip, puerto)
			if(solicitud[0:9] == 'registrar'):
				if(solicitud[10:16] == 'fisico'):
					conexion.replicar_servidor('fisico')
				elif(solicitud[10:18] == 'clearnet'):
					conexion.replicar_servidor('clearnet')
				else:
					conexion.replicar_servidor('nodo')
				conexion.notificar_nodos()
			print('Solicitud atendida para ip: '+ip)
		elif(mensaje == 1):
			print('replicando server...')		
			time.sleep(1)
			conexion.notificar_nodos()	
		elif(len(conexion.lista_nodos) == 0):
			conexion.socket_nodo.close()
			print('Ay papaya de Zelaya \n tus hijos vuelvan \n vamonos perras')
			break	

main()