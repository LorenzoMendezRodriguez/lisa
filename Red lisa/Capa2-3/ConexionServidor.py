import socket 
import time
from scapy.all import *
from scapy.fields import *
from scapy.layers.inet import IP,TCP
conf.color_theme = RastaTheme()
import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
import threading
from simplecrypt import encrypt, decrypt

'''
Tabla Identificadores de solicitudes para el usuario

Registrar = None
nodos = 1
mensaje = cualquier otro
'''


#Clase que levanta al servidor en una red con una direccion bien conocida
class ConexionServidor:
    def __init__(self):
        self.tcp_ip = '172.28.130.97'
        #self.tcp_ip = '127.0.0.1'
        self.puerto = 5000

        self.server_espejo = '172.28.130.152'
        self.puerto_espejo = 5001

        self.base_red = '127.0.'
        self.id_red_raptolexis = '0.'
        self.id_red_lobezna = '1.'

        self.id_red_servidor = '1'



        self.id_nuevo_nodo_lobo = 1
        self.id_nuevo_nodo_raptor = 1

        self.nuevo_puerto_lobo = 5001
        self.nuevo_puerto_raptor = 5001


        self.id_red_lisa = 64
        self.id_nodo_luz = 0
        self.id_nodo_net = 0


        self.socket_nodo = socket.socket(socket.AF_INET, socket.SOCK_STREAM)      
        self.socket_nodo.bind((self.tcp_ip, self.puerto))

        self.lista_nodos = []
        self.lista_llaves = []

    #Simplemente responde  a un nodo
    #Entradas: mensaje = respuesta, ip y puerto donde esta ubicado el nodo que envia la solicitud   
    def responder_solicitud(self, mensaje, ip, puerto):
        socket_enviar = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        socket_enviar.connect((ip, puerto))
        socketsr1 = StreamSocket(socket_enviar)
        packet = Ether() / IP(src=self.tcp_ip, dst=ip) / TCP(sport=self.puerto, dport=puerto) / Raw(load=mensaje)
        socketsr1.send(packet)
        socketsr1.close()
        socket_enviar.close()

    def pertenece(self, ip):
        l_ip = ip.split('.')
        if(l_ip[2] == self.id_red_servidor):
            return True
        else:
            return False

    def generar_chr_aleatorio(self):
        char = chr(random.randint(33,127))
        return char

    def generar_llave(self):
        t_llave = []
        for i in range (0,6):
            t_llave.append(self.generar_chr_aleatorio())
        r_llave = ('').join(t_llave)
        return r_llave

    
    #El estado de espera del servidor, donde solo espera solicitudes conocidas   

    def esperar_solicitud(self):
        self.socket_nodo.listen(5)
        while 1:
            conn, addr = self.socket_nodo.accept()
            print('Escuchando en: {}').format(addr)            
            try:
                data = conn.recv(2048)
        
                if data:                    
                    paquete = scapy.layers.inet.Ether(data)
                    conn.close()
                    return paquete
                   
                else:
                    print("Error al recibir")
            except Exception as server_error:
                print('Error: {}').format(server_error)
                conn.close()

    #Dado un tipo de conexion, genera el tipo de conexion dado por el usuario
    #Tipos: lobezna, raptor, DLA
    
    def generar_nueva_conexion(self, tipo, flag = None):
        nueva_conexion = ''
        if(tipo == 'lobezna'):
            self.id_nuevo_nodo_lobo +=1
            self.nuevo_puerto_lobo += 1
            nuevo_nodo_ip = self.base_red + self.id_red_lobezna + str(self.id_nuevo_nodo_lobo)
            nuevo_nodo_puerto = str(self.nuevo_puerto_lobo)

        elif(tipo == 'raptor'):
            self.id_nuevo_nodo_raptor +=1
            self.nuevo_puerto_raptor +=1
            nuevo_nodo_ip = self.base_red + self.id_red_raptolexis + str(self.id_nuevo_nodo_raptor)
            nuevo_nodo_puerto = str(self.nuevo_puerto_raptor)

        if(flag == 'arduino'):
            self.id_nodo_luz+=1
            id_nodo = 'DLA-' + str(self.id_nodo_luz)

        elif (flag == 'clearnet'):
            self.id_nodo_net += 1
            id_nodo = 'NET' + str(self.id_nodo_net)

        else:
            self.id_red_lisa+=1
            id_nodo = str(chr(self.id_red_lisa)) 
  

        self.lista_nodos.append([id_nodo,nuevo_nodo_ip,nuevo_nodo_puerto])
        noLlave_valida = True
        nueva_llave = ''
        while(noLlave_valida):
            nueva_llave = self.generar_llave()
            if(not(nueva_llave in self.lista_llaves)):
                self.lista_llaves.append(nueva_llave)
                noLlave_valida = False       
        return id_nodo +','+nuevo_nodo_ip + ',' + nuevo_nodo_puerto + ',' + nueva_llave

    #Empaqueta la lista de nodos para enviarla al usuario
    #Formato nid|nip|npt,nid|nip|npt    
    def empaquetar_nodos(self):
        res = self.lista_nodos[0][0] + '|' +self.lista_nodos[0][1] + '|' +self.lista_nodos[0][2]
        for i in range(1,len(self.lista_nodos)):
            res += ',' + self.lista_nodos[i][0] + '|' +self.lista_nodos[i][1] + '|' + self.lista_nodos[i][2]
        return '1'+res
    

    #Remueve una ip de la tabla de routeo
    def remover_usuario(self,ip):
        i=0
        for i in range(0,len(self.lista_nodos)):
            if(self.lista_nodos[i][1] == ip):
                break
        del self.lista_nodos[i]
        del self.lista_llaves[i]

    #notifica a toda su tabla de routeo que se actualizo tu tabla de routeo 
    def notificar_nodos(self):
        for i in self.lista_nodos:
            if(self.pertenece(i[1])):
                time.sleep(2)
                self.responder_solicitud(self.empaquetar_nodos(),i[1],int(i[2]))

    def obtener_llave(self,id):
        for i in range(0,len(self.lista_nodos)):
            if(id == self.lista_nodos[i][0]):
                return self.lista_llaves[i]


    #Encipta el mensaje escrito por el usuario con la llave destino, manteniendo su camino intacto
    #formato del mensaje: camino----id_destinoMensaje
    def encriptar_mensaje(self,mensaje):
        l_mensaje = mensaje.split('----')
        camino = l_mensaje[0]
        msj = l_mensaje[1]
        l_camino = camino.split(',')
        t_id = l_camino[len(l_camino)-1]
        llave = self.obtener_llave(t_id)
        texto_encriptado = encrypt(llave, msj)
        ret = camino + '----' + texto_encriptado
        return ret
        
    def replicar_servidor(self,tipo):
        if(tipo == 'fisico'):
            self.responder_solicitud('replicarDLA',self.server_espejo, self.puerto_espejo)
        elif(tipo == 'clearnet'):
            self.responder_solicitud('replicarNet',self.server_espejo, self.puerto_espejo)
        else:
            self.responder_solicitud('replicar',self.server_espejo, self.puerto_espejo)

            

    def atender_solicitud(self,paquete):
        solicitud =  paquete[Raw].load
        print('********',solicitud)
        if(solicitud == 'registrar_lobezna'):
            nueva_con = self.generar_nueva_conexion('lobezna')
            return nueva_con
        elif(solicitud == 'registrar_raptor'):
            nueva_con = self.generar_nueva_conexion('raptor')
            return nueva_con
        elif(solicitud == 'registrar_medio_fisico'):
            nueva_con = self.generar_nueva_conexion('arduino')
            return nueva_con
        elif(solicitud == 'registrar_fisico_raptor'):
            nueva_con = self.generar_nueva_conexion('raptor','arduino')
            return nueva_con
        elif(solicitud == 'registrar_fisico_loba'):
            nueva_con = self.generar_nueva_conexion('lobezna','arduino')
            return nueva_con
        elif(solicitud == 'registrar_clearnet_raptor'):
            nueva_con = self.generar_nueva_conexion('raptor','clearnet')
            return nueva_con
        elif(solicitud == 'registrar_clearnet_loba'):
            nueva_con = self.generar_nueva_conexion('lobezna','clearnet')
            return nueva_con
        elif(solicitud[:8] == 'replicar'):
            if(solicitud[8:] == 'Net'):
                nueva_con = self.generar_nueva_conexion('raptor','clearnet')
                return 1
            elif(solicitud[8:] == 'DLA'):
                nueva_con = self.generar_nueva_conexion('raptor','arduino')
                return 1
            else:
                nueva_con = self.generar_nueva_conexion('raptor')
                return 1
        elif(solicitud == 'nodos'):
            l_nodos = self.empaquetar_nodos()
            return l_nodos        
        elif(solicitud[:9] == 'encriptar'):
            mensaje_encriptado = self.encriptar_mensaje(solicitud[9:])
            return mensaje_encriptado
        elif(solicitud == 'dc'):
            t_ip = paquete[IP].src
            self.remover_usuario(t_ip)
            print('Removido usuario ip: ' + t_ip)
            return -1

        
        else:
            print('Mae di, no es valida la solicitud, se hecho la vara')
