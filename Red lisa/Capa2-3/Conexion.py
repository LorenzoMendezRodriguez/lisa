import socket 
import time
from scapy.all import *
from scapy.fields import *
from scapy.layers.inet import IP,TCP
from colorama import *
conf.color_theme = RastaTheme()
import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
import threading
from simplecrypt import encrypt, decrypt

import sys
sys.path.append('../MedioFisico')
from DispositivoLuzAdaptador import *


#Clase que levanta una conexion sencilla para Lisa
#Se levanta en el local host .99
#Puerto 5099


class Conexion:
    def __init__(self):
        init()
        self.server_conocido = '172.28.130.97'
        #self.server_conocido = '127.0.0.1'
        self.puerto_server_conocido = 5000

        self.tcp_ip = '127.0.0.99'
        self.puerto = 5099

        self.socket_nodo = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket_nodo.bind((self.tcp_ip, self.puerto))


        self.nombre_red = ''
        self.id_red = ''
        self.lista_nodos = []
        self.llave = ''

        self.DLA = DispositivoLuzAdaptador()



#Envia una solicitud al servidor que se encuentra en una direccion bien conocida
#Recibe una de las acciones validas
#Acciones Validas= Registar-Red / nodos/dc 

    def enviar_solicitud(self, accion):
        socket_enviar = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        socket_enviar.connect((self.server_conocido, self.puerto_server_conocido))
        socketsr1 = StreamSocket(socket_enviar)
        packet = Ether() / IP(src=self.tcp_ip, dst=self.server_conocido) / TCP(dport=self.puerto_server_conocido, sport=self.puerto) / Raw(load=accion)
        socketsr1.send(packet)
        socketsr1.close()
        socket_enviar.close()

#Dado un id, te devuelve el puerto tcp y el ip donde se encuentra el nodo
    def obtener_credenciales(self,nodo_id):
        for i in self.lista_nodos:
            if(i[0] == nodo_id):
                return i[1],int(i[2])
        return 0,0

#Dado im ip, te devuelve el ide del nodo
    def obtener_id(self,ip):
        for i in self.lista_nodos:
            if(i[1] == ip):
                return i[0]
        return -1

#Aca dada la op, se analiza a cual de las 2 redes pertence cada nodo en la tabla de routeo
    def obtener_nombre(self,ip):
        l_ip = ip.split('.')
        if(l_ip[2] == self.id_red):
            return self.nombre_red
        else:
            return 'Desconocida'

#Simplemente parsea los nodos que viene desde el servidor bajo el estandard impuesto por nosotros
#Estandard de entrada: 'nod1|..|..,nodo2|...|..' 
    def parsear_nodos(self,nodos):
        t_elementos = nodos.split(',')
        t_nodos = []
        for i in t_elementos:
            t_nodos.append(i.split('|'))
        return t_nodos

#Imprime los nodos en pantalla dada una lista de nodos
    def imprimir_nodos(self,l_nodos):
        print(Fore.CYAN)
        for i in l_nodos:
            print('Nodo id:'+i[0] + ' |'+ ' Red: ' + self.obtener_nombre(i[1]))
        print(Style.RESET_ALL)

#Desempaqueta el mensaje
#Formato: camino----mensaje
    def desempaquetar_mensaje(self,mensaje_empaquetado):
        l_temp = mensaje_empaquetado.split('----')
        camino = l_temp[0]
        mensaje = l_temp[1]
        return camino,mensaje

#Empaqueta un mensaje tipo Lisa, esto para rebotar la informacion para asegurarnos anonimato
    def empaquetar_mensaje_lisa(self, mensaje, camino):
        return camino + '----' + mensaje
#Este simplemente returna una lista del camino
    def parsear_camino_lista(self, camino):
        return camino.split(',')
#Empaqueta el camino para que sea lisa like
    def empaquetar_camino(self, l_camino):
        return (',').join(l_camino)

#Envia el mensaje hacia un nodo Lisa-Like
#Parametros: Camino = A,B,C , Mensaje = mensaje lisa-like, string=id-destino-final/string=mensaje
    def enviar_mensaje_Lisa(self,mensaje,camino):
        t_camino = self.parsear_camino_lista(camino)
        nodo_siguiente = t_camino[0]
        flag_arduino = False
        print(nodo_siguiente)

        if(len(nodo_siguiente) == 5):
            #del t_camino[0] #delete DLA
            #nodo_siguiente = t_camino[0]
            print('arduino',nodo_siguiente)
            flag_arduino = True
        
        del t_camino[0]

        print(t_camino,flag_arduino)
        nuevo_camino = self.empaquetar_camino(t_camino)
        ip,puerto = self.obtener_credenciales(nodo_siguiente)
        msj_paq = self.empaquetar_mensaje_lisa(mensaje,nuevo_camino)
        print(nodo_siguiente)
        print(ip)
        print(puerto)
        packet = Ether() / IP(src=self.tcp_ip, dst=ip) / TCP(sport=self.puerto,dport=puerto) / Raw(load=msj_paq)

        
        if(flag_arduino):
            self.DLA.enviar_mensaje(str(packet))

        else:
            socket_enviar = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            socket_enviar.connect((ip, puerto))
            socketsr1 = StreamSocket(socket_enviar)
            socketsr1.send(packet)
            socketsr1.close()
            socket_enviar.close()

#FUNCION ESCUCHA para obtener informacion
    def recibir_datos(self):
        self.socket_nodo.listen(5)
        conn, addr = self.socket_nodo.accept()       
        while 1:           
            try:
                data = conn.recv(2048)
                if data:
                    paquete = scapy.layers.inet.Ether(data)
                    conn.close()
                    return paquete
                else:
                    break
            except Exception as server_error:
                print('Error: {}').format(server_error)
                conn.close()

#Funcion que identifica las redes nuevas, esto lo deberia saber el server es por comodidad
    def identificar_red(self,ip):
        l_id = ip.split('.')
        if(l_id[2] == '0'):
            return 'Raptolexis-Lisa','0' 
        else:
            return 'Lobezna-Lisa','1'

    def guardar_llave(self,llave):
        self.llave = llave

    def decriptar(self, mensaje):
        msj_decriptado = decrypt(self.llave, mensaje)
        return msj_decriptado


#Dado sus nuevas, credenciales se conecta a la red Lisa.
    def conectar_a_Lisa(self, ip, puerto):
        self.socket_nodo.close()
        time.sleep(1)
        self.tcp_ip = ip
        self.puerto = puerto
        self.nombre_red, self.id_red = self.identificar_red(ip) 
        self.socket_nodo = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket_nodo.bind((ip, puerto))


   