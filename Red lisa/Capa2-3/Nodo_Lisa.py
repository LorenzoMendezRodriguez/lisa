from Conexion import *



conexion =  Conexion()

ID_NODO = 'X'

verbose = False



#Imprime el mensaje lisa-like
def parsear_mensaje(mensaje_con_id):
	mensaje = mensaje_con_id[1:len(mensaje_con_id)]
	nodo_id = mensaje_con_id[:1]
	return nodo_id, mensaje


#Funcion que se encarga de la escucha del nodo
def esperar_acciones():
	global conexion,verbose
	while(1):
		paquete = conexion.recibir_datos()
		mensaje_paq_full = paquete[Raw].load
		ip_source = paquete[IP].src
		if(conexion.server_conocido == ip_source):
			id_mensaje_paq = mensaje_paq_full[:1]
			mensaje_paq = mensaje_paq_full[1:]

			if(id_mensaje_paq == '1'):				
				print('\n' + Fore.RED + '!Notify')
				print('Nodos en Lisa:')
				t_nodos = conexion.parsear_nodos(mensaje_paq)
				conexion.lista_nodos = t_nodos
				conexion.imprimir_nodos(t_nodos)
			else:
				camino,mensaje_con_id = conexion.desempaquetar_mensaje(mensaje_paq_full)
				conexion.enviar_mensaje_Lisa(mensaje_con_id,camino)
				print(Fore.BLUE +'\nPaquete Enviado!!')
				print(Style.RESET_ALL)
		else:
			camino,mensaje_con_id = conexion.desempaquetar_mensaje(mensaje_paq_full)	
				
			if(camino == ''):
				try:
					mensaje_plano_con_id=conexion.decriptar(mensaje_con_id)
				except:
					mensaje_plano_con_id=mensaje_con_id				
				nodo_id,mensaje = parsear_mensaje(mensaje_plano_con_id)	
				print('\n' + Fore.MAGENTA +'Nodo - ' + nodo_id + ': '+Fore.GREEN + mensaje)
				print(Style.RESET_ALL)
			else:
				print(Fore.YELLOW +'\nAh mira un paquete!!! No es mio :c')
				print(Style.RESET_ALL)
				if(verbose == True):
					print('El paquete lleva el siguiente mensaje: \n' + mensaje_con_id)
				conexion.enviar_mensaje_Lisa(mensaje_con_id,camino)


#Identifica a que red pertence, podria ser distinto pero por comodidad esta aca	
def identificar_red(ip):
	l_id = ip.split('.')
	if(l_id[2] == '0'):
		return 'Raptolexis-Lisa'
	else:
		return 'Lobezna-Lisa'



#Flujo del programa, que primero se conectara a la red para despues integrarse y hablar con el usuario de que va a hacer
def main():
	global conexion, verbose
	if(len(sys.argv) > 1):
		if(sys.argv[1] == '-v'):
			verbose = True
			print(Fore.GREEN + 'verbose mode' + Style.RESET_ALL)
	print('Favor digite al tipo de red Lisa que desea conectarse')
	tipo_red = raw_input('0 - Red Raptolexis / 1 - Red Lobezna\n')
	if(int(tipo_red)):
		conexion.enviar_solicitud('registrar_lobezna')	
	else:
		conexion.enviar_solicitud('registrar_raptor')
	paquete = conexion.recibir_datos()
	mensaje = paquete[Raw].load.split(',')
	ID_NODO = mensaje[0]
	print('Conectado a ' +  identificar_red(mensaje[1]) +' |'+ ' id: '+ ID_NODO +' |' +' ip:' +mensaje[1] + ' |' +' pt:' + mensaje[2] + ' | ' + 'llave: ' + mensaje[3])
	conexion.conectar_a_Lisa(mensaje[1],int(mensaje[2]))
	conexion.guardar_llave(mensaje[3])
	demonio_recepcion = threading.Thread(target=esperar_acciones, args=())
	demonio_recepcion.daemon = True 
	demonio_recepcion.start()
	t = 1
	while(t):
		time.sleep(3)
		print('\nBienvenido al chat, nodo : ' + ID_NODO + ' | ip: '+mensaje[1])
		print('Si digita 1, tendra otras opciones')
		nodo_id = raw_input('ID nodo destino : ')
		bandera,bandera2 = conexion.obtener_credenciales(nodo_id)
		if(bandera == 0):
			print('\nComandos disponibles: nodos / dc \n' + 'nodos = Lista de nodos en Lisa\n' + 'dc = Desconectarse de Lisa')
			solcitud = raw_input( 'Digite un comando \n')

			conexion.enviar_solicitud(solcitud)
			if(solcitud == 'dc'):
				conexion.socket_nodo.close()
				print('Abandonando Lisa.....')
				t=0

		else:
			bandera_camino = raw_input('Digite [Z] para armar un camino:')
			camino = ''
			if(bandera_camino == 'Z'):
				print('Formato-Camino: B,E,D,F')
				print('El ejemplo camino es un camino valido entre A y G')
				camino = raw_input('Digite el camino entre este nodo y su destino:\n')
				camino += ','+nodo_id

			else:
				camino = nodo_id
			msj = ID_NODO	
			msj += raw_input('Digite el mensaje: ')
			sol_mensaje = camino +'----'+msj
			decision_encriptar = raw_input('Digite \'enc\' para encriptar su mensaje:')
			if(decision_encriptar == 'enc'):
				conexion.enviar_solicitud('encriptar' + sol_mensaje)
				print('Protegiendo su informacion ......')
				time.sleep(2)
				print('Enviando mensaje a ID: ' + nodo_id + '\n')
				time.sleep(1)
			else:
				print('Enviando mensaje a ID: ' + nodo_id + '\n')
				conexion.enviar_mensaje_Lisa(msj,camino)
				time.sleep(1)
				print(Fore.BLUE + 'Mensaje Enviado!!' + Style.RESET_ALL)



main()