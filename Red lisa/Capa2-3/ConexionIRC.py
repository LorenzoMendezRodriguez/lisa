import socket
import sys
import threading
import time

class ConexionIRC:
	def __init__(self):
		self.server = "irc.freenode.net"      
		self.channel = "##Lisa"
		self.botnick = "Red-Lisa"


	#Realiza la conexion con el servidor IRC
	def irc_conexion(self):
		self.irc = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #definir socket
		#print "Conectando con el servidor: "+self.server
		self.irc.connect((self.server, 6667)) #conexion con el servidor
		#Autenticar usuario
		self.irc.send("USER "+ self.botnick +" "+ self.botnick +" "+ self.botnick +" :This is a fun bot!\n") 
		self.irc.send("NICK "+ self.botnick +"\n") #set nick
		self.irc.send("PRIVMSG nickserv :iNOOPE\r\n") #autenticar
		self.irc.send("JOIN "+ self.channel +"\n") #unirse al canal

		while 1:    
		   text=self.irc.recv(2040)  #recibiendo informacion
		   #print text  

		   if text.find('PING') != -1: #revisar si 'PING' esta funcionando
		      self.irc.send('PONG ' + text.split() [1] + '\r\n') #Retorna Pong al servidor

		   if text.find(':hi') !=-1: #si entro hi
		    t = text.split(':hi') #cambiar
		    to = t[1].strip() #obtiene la primera palabra despues de hi
		    irc.send('PRIVMSG '+self.channel+' :Hello '+str(to)+'! \r\n')

	#Esta funcion lanza el mensaje a clearnet
	def publicar_chat(self,mensaje):
		self.irc.send('PRIVMSG '+self.channel+' :' + mensaje +' \r\n')

	#Levanta la conexion con el servidor IRC
	def iniciar_irc_conexion(self):
		demonio_recepcion = threading.Thread(target=self.irc_conexion, args=())
		demonio_recepcion.daemon = True 
		demonio_recepcion.start()

