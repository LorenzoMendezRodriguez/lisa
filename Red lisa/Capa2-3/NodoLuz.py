import socket 
import time
from scapy.all import *
from scapy.fields import *
from scapy.layers.inet import IP,TCP
from colorama import *
conf.color_theme = RastaTheme()
import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
import threading

import sys
sys.path.append('../MedioFisico')
from DispositivoLuzAdaptador import *

class NodoLuz:
    def __init__(self):
        self.lista_nodos = [['A','127.0.1.2','5002'],['B','127.0.1.3','5003'],['C','127.0.1.4','5004']]
        
    def obtener_credenciales(self,nodo_id):
        for i in self.lista_nodos:
            if(i[0] == nodo_id):
                return i[1],int(i[2])
        return 0,0

    def desempaquetar_mensaje(self,mensaje_empaquetado):
        l_temp = mensaje_empaquetado.split('----')
        camino = l_temp[0]
        mensaje = l_temp[1]
        return camino,mensaje

    def empaquetar_mensaje_lisa(self, mensaje, camino):
        return camino + '----' + mensaje

    def parsear_camino_lista(self, camino):
        return camino.split(',')

    def empaquetar_camino(self, l_camino):
        return (',').join(l_camino)

    def enviar_mensaje_Lisa(self,mensaje,camino,ip_src,sport):
        print(mensaje,camino)
        t_camino = self.parsear_camino_lista(camino)
        nodo_siguiente = t_camino[0]
        del t_camino[0]
        nuevo_camino = self.empaquetar_camino(t_camino)
        ip,puerto = self.obtener_credenciales(nodo_siguiente)

        print(ip,puerto)
        msj_paq = self.empaquetar_mensaje_lisa(mensaje,nuevo_camino)
        socket_enviar = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        socket_enviar.connect((ip, puerto))
        socketsr1 = StreamSocket(socket_enviar)
        packet = Ether() / IP(src=ip_src, dst=ip) / TCP(sport=sport,dport=puerto) / Raw(load=msj_paq)
        socketsr1.send(packet)
        socketsr1.close()
        socket_enviar.close()

    def enviar_mensaje_Arduino(self,mensaje,camino):
        t_camino = self.parsear_camino_lista(camino)
        nodo_siguiente = t_camino[0]
        del t_camino[0]
        nuevo_camino = self.empaquetar_camino(t_camino)
        ip,puerto = self.obtener_credenciales(nodo_siguiente)
        msj_paq = self.empaquetar_mensaje_lisa(mensaje,nuevo_camino)
        packet = Ether() / IP(src=self.tcp_ip, dst=ip) / TCP(sport=self.puerto,dport=puerto) / Raw(load=msj_paq)
        packet.show()
        #Enviando por arduino
        enviar_mensaje(str(packet))
    