from Conexion import *
from ConexionIRC import * 
import sys

conexion =  Conexion()
conexionIRC = ConexionIRC()

ID_NODO = 'X'


#Recibe el mensaje con el id ejemplo -> Amensaje
#Retorna id y mensaje separados ejemplo -> A, mensaje
def parsear_mensaje(mensaje_con_id):
	mensaje = mensaje_con_id[1:len(mensaje_con_id)]
	nodo_id = mensaje_con_id[:1]
	return nodo_id, mensaje


def esperar_acciones():
	global conexion
	while(1):
		time.sleep(1)
		paquete = conexion.recibir_datos()
		mensaje_paq = paquete[Raw].load
		ip_source = paquete[IP].src
		#Si la conexion es diferente a la del servidor
		#No necesito nada del servidor
		if(conexion.server_conocido != ip_source):
			camino,mensaje_con_id = conexion.desempaquetar_mensaje(mensaje_paq)	

			nodo_id,mensaje = parsear_mensaje(mensaje_con_id)

			mensaje = nodo_id + ': '+ mensaje
			
			#Publica el mensaje en clearnet
			conexionIRC.publicar_chat(mensaje)
		
	
def identificar_red(ip):
	l_id = ip.split('.')
	if(l_id[2] == '0'):
		return 'Raptolexis-Lisa'
	else:
		return 'Lobezna-Lisa'

def main():
	global conexion

	#Realiza la conexion con el servidor IRC
	conexionIRC.iniciar_irc_conexion()

	print('Favor digite al tipo de red Lisa que desea conectarse')
	tipo_red = raw_input('0 - Red Raptolexis / 1 - Red Lobezna\n')

	if(int(tipo_red)):
		conexion.enviar_solicitud('registrar_clearnet_loba')
	else:
		conexion.enviar_solicitud('registrar_clearnet_raptor')
		

	paquete = conexion.recibir_datos()
	mensaje = paquete[Raw].load.split(',')
	ID_NODO = mensaje[0]

	print('Conectado a ' +  identificar_red(mensaje[1]) +' |'+ ' id: '+ ID_NODO +' |' +' ip:' +mensaje[1] + ' |' +' puerto:' + mensaje[2])
	
	conexion.conectar_a_Lisa(mensaje[1],int(mensaje[2]))
	esperar_acciones()
	

main()