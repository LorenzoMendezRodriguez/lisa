from Conexion import *
import sys
conexion =  Conexion()
ID_NODO = 'X'


#Recibe el mensaje con el id ejemplo -> Amensaje
#Retorna id y mensaje separados ejemplo -> A, mensaje
def parsear_mensaje(mensaje_con_id):
	mensaje = mensaje_con_id[1:len(mensaje_con_id)]
	nodo_id = mensaje_con_id[:1]
	return nodo_id, mensaje

#Recibe datos del servidor
def esperar_acciones():
	global conexion
	while(1):
		paquete = conexion.recibir_datos()
		mensaje_paq = paquete[Raw].load
		ip_source = paquete[IP].src
		if(conexion.server_conocido == ip_source):
			print('\n' + Fore.RED + '!Notify')
			print('Nodos en Lisa:')
			t_nodos = conexion.parsear_nodos(mensaje_paq[1:])
			conexion.lista_nodos = t_nodos
			conexion.imprimir_nodos(t_nodos)


def recibiendo_por_arduino():
	mensaje_completo = 0
	global conexion,recibir
	while True:
		time.sleep(1)
		#mensaje_completo = recibir.recibiendo_mensaje()
		mensaje_completo = conexion.DLA.recibiendo_mensaje()
		
		if(mensaje_completo != 'Error'):
			paqueteScapy = scapy.layers.inet.Ether(mensaje_completo)

			mensaje_paquete = paqueteScapy[Raw].load

			camino,mensaje_con_id = conexion.desempaquetar_mensaje(mensaje_paquete)

			conexion.enviar_mensaje_Lisa(mensaje_con_id,camino)

		
def identificar_red(ip):
	l_id = ip.split('.')
	if(l_id[2] == '0'):
		return 'Raptolexis-Lisa'
	else:
		return 'Lobezna-Lisa'

def main():
	global conexion

	print('Favor digite al tipo de red Lisa que desea conectarse')
	tipo_red = raw_input('0 - Red Raptolexis / 1 - Red Lobezna\n')

	#Levanto thread para simpre estar recibiendo por arduino
	demonio_recepcion = threading.Thread(target=recibiendo_por_arduino, args=())
	demonio_recepcion.daemon = True 
	demonio_recepcion.start()

	if(int(tipo_red)):
		conexion.enviar_solicitud('registrar_fisico_loba')
	else:
		conexion.enviar_solicitud('registrar_fisico_raptor')

	paquete = conexion.recibir_datos()
	mensaje = paquete[Raw].load.split(',')
	ID_NODO = mensaje[0]

	print('Conectado a ' +  identificar_red(mensaje[1]) +' |'+ ' id: '+ ID_NODO +' |' +' ip:' +mensaje[1] + ' |' +' puerto:' + mensaje[2])
	
	conexion.conectar_a_Lisa(mensaje[1],int(mensaje[2]))
	esperar_acciones()
	

main()
