/*
    CODIGO PARA ARDUINO
*/
 
#define LED_A 11
int lightPin_A = 1;

 
int mssg = 0; //variable para guardar el mensaje
  
void setup(){
   pinMode(LED_A, OUTPUT); //establecemos el pin D11 como salida
   Serial.begin(9600); //inicializamos Serial
}
  
void loop(){
   int lectura_A = analogRead(lightPin_A); // lee la entrada análoga
   Serial.println(lectura_A);// escribe en el puerto serial

   delay(500); // delay para poder ver los datos
   
   if (Serial.available() > 0){
      mssg = Serial.read(); //leemos el serial
     
      if(mssg == '1'){
         digitalWrite(11, HIGH); //si entra una 'A' encendemos el LED
      }
      if(mssg == '0'){
         digitalWrite(11, LOW); //si entra una 'a' apagamos el LED
      }
   }
}
