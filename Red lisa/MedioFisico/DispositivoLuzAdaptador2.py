"""
    PROGRAMA DE PYTHON
    
"""
import serial
import time
import threading
import binascii 
import crc16


from scapy.all import *
from scapy.fields import *
from scapy.layers.inet import IP,TCP
from colorama import *
conf.color_theme = RastaTheme()
import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
 
#Inicializamos el puerto de serie a 9600 baud
ser = serial.Serial('/dev/ttyUSB0', 9600)
ruta_nodos = []

# envia una cadena de binarios a traves del arduino
#Entrada una cadena de binarios y el led por el cual enviara

class test:
	def __init__(self):
		self.main()

	def send_arduino(self,binario):
		#inicio de la trama
		ser.write('1') 

		for bit in binario:
			if bit == '1':
			    ser.write('1')
			    time.sleep(0.50)
			else:
				ser.write('0')
				time.sleep(0.50)

		#apago el led, mantener apagado			
		time.sleep(0.50)
		ser.write('0')



	def receive_arduino(self,cantidad_de_bits):
		bit = None
		flag = False
		cont = 0
		mensaje = ''
		while True:

			bit = ser.readline()

			if(cont == cantidad_de_bits + 1):
				break

			if (int(bit) <= 1000):	
				flag=True #si entro un 1, quiere decir que voy a recibir un paquete, mensaje
				cont+=1
				mensaje += '1'

			else:
				if (flag):
					cont+=1
					mensaje += '0'
		return mensaje


	def recibiendo_mensaje(self):
		mensaje_completo = ''
	
		mensaje_de_configuracion = self.receive_arduino(32)
		mensaje_de_configuracion = mensaje_de_configuracion[1:len(mensaje_de_configuracion)]

		largo_mensaje = mensaje_de_configuracion[:16]
		#print(largo_mensaje)

		largo_mensaje = int(largo_mensaje,2)
		#time.sleep(1)

		print("Voy a recibir -> ",largo_mensaje, " bits")

		while largo_mensaje >= 300:
			mensaje = self.receive_arduino(300)
			mensaje_completo += mensaje[1:len(mensaje)]
			largo_mensaje -= 300

		mensaje = self.receive_arduino(largo_mensaje)
		mensaje_completo += mensaje[1:len(mensaje)]

		checksum = mensaje_de_configuracion[16:]
		checksum = int(checksum,2)

		if(checksum == crc16.crc16xmodem(mensaje_completo)):
			print("Paquete recibido con exito")
			mensaje_completo = binascii.unhexlify('%x' % int(('0b'+mensaje_completo), 2))
	
		else:
			print("Error al recibir paquete")
			mensaje_completo = 'Error'
			
		return mensaje_completo		




	def crear_ruta_binaria(self,nodos):
		NODOS_BINARIOS = []

		for nodo in nodos:
			nodo_binario = bin(int(binascii.hexlify(nodo),16)).replace("0b", "") 
			NODOS_BINARIOS.append(nodo_binario)
		return NODOS_BINARIOS


	def enviar_mensaje(self,mensaje):
		mensaje = bin(int(binascii.hexlify(mensaje),16)).replace("0b", "")
		largo_mensaje = '{0:10b}'.format(len(mensaje))

		#checksum
		crc = crc16.crc16xmodem(mensaje)
		size_crc = '{0:16b}'.format(crc)

		print(largo_mensaje)

		mensaje_de_configuracion = largo_mensaje + size_crc
		self.send_arduino(mensaje_de_configuracion)
		time.sleep(2)

		if(len(mensaje) <= 1024):
			self.send_arduino(mensaje)
			time.sleep(2)

		else:
			len_mensaje = len(mensaje)
			print('Voy a enviar -> ',len_mensaje, 'bits')
			#print(mensaje)
			while len_mensaje >= 128:
				#print(mensaje[:128])
				self.send_arduino(mensaje[:128])
				time.sleep(30)

				mensaje = mensaje[128:]
				len_mensaje-= 128

			self.send_arduino(mensaje)
			time.sleep(20)
			#print(mensaje)
		fin = '{0:10b}'.format(1) 
		self.send_arduino(fin)


	def enviar_ruta(self,nodos):
		NODOS_BINARIOS = crear_ruta_binaria(nodos)

		for nodo in NODOS_BINARIOS:
			largo_node_binario = '{0:10b}'.format(70) 
			crc = crc16.crc16xmodem(nodo)
			size_crc = '{0:16b}'.format(crc)

			mensaje = largo_node_binario + size_crc
			send_arduino(mensaje)
			time.sleep(1)

			send_arduino(nodo)
			time.sleep(25) #Para que logre leer el siguiente mensaje

		#Envio un 1 para indicar que el mensaje ya se envio por completo
		fin = '{0:10b}'.format(1) 
		send_arduino(fin)


	def main(self):
		#recibiendo = threading.Thread(target=recibiendo_mensaje)
		#recibiendo.start()

		#time.sleep(5)#Si inicia muy rapido puede que no logre leer algun bit
		#hola = recibiendo_mensaje2()
		
		#NODOS = ['127.0.0.1','127.0.0.2','127.0.0.3','127.0.0.4','127.0.0.5']

		#enviar_ruta(NODOS)
		#while True:
		mensaje = "Digita el mensaje"
		self.enviar_mensaje(mensaje)

		
		#mensaje = 'Lorenzo es toda la repicha ok vamos a ver que tal'
		#mensaje = bin(int(binascii.hexlify(mensaje),16)).replace("0b", "")
		#enviar_mensaje(mensaje)
		#mensaje = bin(int(binascii.hexlify(mensaje),16)).replace("0b", "")
		#print('{0:10b}'.format(len(mensaje)))

		#mensaje = bin(int(binascii.hexlify('Lorenzo everything is fine hola'),16)).replace("0b", "")
		#print('{0:10b}'.format(len(mensaje)))
		'''
		len_msg = len(mensaje)
		while len_msg >= 128:
			print(mensaje[:128] + '\n')
			mensaje = mensaje[128:]
			len_msg-= 128
		print('final',mensaje)
		'''

	#main()
test()