"""
    PROGRAMA DE PYTHON
    
"""
import serial
import time
import threading
import binascii 
import crc16
 

class DispositivoLuzAdaptador:
	def __init__(self):
		#Inicializamos el puerto de serie a 9600 baud comunicacion con el arduino
		self.ser = serial.Serial('/dev/ttyUSB1', 9600)

	#Envia una cadena de binarios a traves del arduino
	#Entrada una cadena de binarios
	#Salida enciende y apaga un led
	def send_arduino(self,binario):
		#inicio de la trama
		self.ser.write('1') 
		for bit in binario:
			
			if bit == '1': #led encendido
			    self.ser.write('1')
			else: #led apagado
				self.ser.write('0')
			time.sleep(0.50)# esperar 0.50 antes de cada accion

		#apago el led, mantener apagado			
		self.ser.write('0')


	"""
	Esta funcion se encarga de recibir el mensaje a traves de la foto-resistencia
	Entrada cantidad de bits que va a recibir
	Salida mensaje binario recibido
	"""
	def receive_arduino(self,cantidad_de_bits):
		bit = None
		flag = False
		cont = 0
		mensaje = ''


		while True:

			bit = self.ser.readline()


			#Si contador es igual a la cantidad de bits que debia recibir se detiene
			if(cont == cantidad_de_bits + 1):
				break

			if (int(bit) <= 1000):	
				flag=True #si entro un 1, quiere decir que voy a recibir un paquete
				cont+=1
				mensaje += '1'

			else:
				if (flag):
					cont+=1
					mensaje += '0'
		return mensaje

	"""
	Esta funcion se encarga de recibir los mensajes por la foto-resitencia no importa el
	tama;o 
	No tiene ninguna entrada
	Salida el mensaje completo que recibio 
	"""
	def recibiendo_mensaje(self):
		mensaje_completo = ''
	
		mensaje_de_configuracion = self.receive_arduino(32)
		mensaje_de_configuracion = mensaje_de_configuracion[1:len(mensaje_de_configuracion)]

		largo_mensaje = mensaje_de_configuracion[:16]
		#print(largo_mensaje)

		largo_mensaje = int(largo_mensaje,2)
		#time.sleep(1)

		print("Voy a recibir -> ",largo_mensaje, " bits")

		while largo_mensaje >= 300:
			mensaje = self.receive_arduino(300)
			mensaje_completo += mensaje[1:len(mensaje)]
			largo_mensaje -= 300

		mensaje = self.receive_arduino(largo_mensaje)
		mensaje_completo += mensaje[1:len(mensaje)]

		checksum = mensaje_de_configuracion[16:]
		checksum = int(checksum,2)

		if(checksum == crc16.crc16xmodem(mensaje_completo)):
			print("Paquete recibido con exito")
			mensaje_completo = binascii.unhexlify('%x' % int(('0b'+mensaje_completo), 2))
	
		else:
			print("Error al recibir paquete")
			mensaje_completo = 'Error'
			
		return mensaje_completo	
			

	"""
	Esta funcion se encarga de recibir un mensaje convertirlo a binario
	y enviarlo por el medio fisico luz led
	Entradas recibe un mensaje de tipo string
	Salidas envia un mensaje con ayuda de la funcion send_arduino
	"""
	def enviar_mensaje(self,mensaje):
		mensaje = bin(int(binascii.hexlify(mensaje),16)).replace("0b", "")
		largo_mensaje = '{0:16b}'.format(len(mensaje))
		#print(largo_mensaje)

		
		crc = crc16.crc16xmodem(mensaje) #checksum
		size_crc = '{0:16b}'.format(crc)

		len_mensaje = len(mensaje)
		print('Voy a enviar -> ',len_mensaje, 'bits')

		mensaje_de_configuracion = largo_mensaje + size_crc
		self.send_arduino(mensaje_de_configuracion)
		time.sleep(5)

		while len_mensaje >= 300:
			self.send_arduino(mensaje[:300])
			mensaje = mensaje[300:]
			len_mensaje-= 300
			time.sleep(5)

		self.send_arduino(mensaje)
		print("Paquete enviado")

