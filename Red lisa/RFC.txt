﻿Redes ii-2019																                                																	    L. Mendez
Categoría: Tecnologías Información											                    A. Torres
-------------------------------------------------------------------------------------

RFC  							IP - Standard									Fecha
Lisa							127.0								   Noviembre 2019
---------------------------------------------------------------------------- [page 1]


Tabla Contenidos

1. Introducción.....................................................................2
2. Diagrama de la red Lisa..........................................................2
3. Requerimientos...................................................................3
4. Terminología.....................................................................3
5. Operación General................................................................4
6. Identificadores generales de nodos...............................................5
6.1 ID..............................................................................5
6.2 Camino..........................................................................5
7. Capa Física......................................................................5
8. Solicitudes al servidor..........................................................5
9. Identificar nodos................................................................5
10. Conexiones entre nodos..........................................................6
11. Decisiones dentro de los nodos..................................................6
12. Redirección entre nodos.........................................................6
13. Funcionamiento del nodo DLA.....................................................6
14. Paso de información.............................................................7
14.1 Nodo a Server..................................................................7
14.1.1 Solicitudes..................................................................7
14.2 Server a Server................................................................7
14.3 Replicar.......................................................................7
14.4 Server a Nodo..................................................................7
14.4.1 Registrar....................................................................7
14.4.2 nodos........................................................................8
14.4.3 dc...........................................................................8
14.4.4 ”encriptar”+camino+delimitador+mensaje.......................................8
14.5 Nodo a Nodo....................................................................8
14.5.1 Mensaje entre la misma red...................................................8
14.6 Nodo Lisa a Nodo DLA y viceversa...............................................8
14.7 Nodo DLA a Nodo DLA............................................................9
14.8 Nodo Lisa a Nodo Clearnet......................................................9



RFC  							IP - Standard									Fecha
Lisa							127.0								   Noviembre 2019
-----------------------------------------------------------------------------[page 2]

1. Introducción

Este documento describe la implementación y el funcionamiento de la red Lisa.
Consiste en una red mesh (cada nodo puede comunicarse con cualquier otro nodo
en la red).  La característica principal de esta red de comunicaciones distribuida
es que el encaminamiento de los mensajes intercambiados entre los usuarios no
revele su identidad.  Así también en la red Lisa pueden existir distintas ciudades
estas se pueden encontrar separadas por muchos kilómetros de distancia por lo
que la red Lisa cuenta con puertas de enlacé entre las ciudades donde opera.
Estas puertas de enlace se manejan mediante un medio físico en este caso la luz,
todo mensaje que sea enviado de una ciudad a otra deberá pasar a través del
medio físico.

2. Diagrama de la red Lisa

                +---+                                               +---+  
        +------>|IRC|                                               |IRC|<---------+
        |       +---+                                               +---+          |
   +----+---+                                                                  +---+----+
+->|Clearnet|                                                                  +Clearnet|<--+
|  +--------+   +--------------+                              +-------------+  +--------+   |
+-------+       | Server Raptor+------------------------------+ Server Lobo |         +-----+
        |     +--+-+---+---+---+                              ++---+------+-+         |
        |     |    |   |   |                                   |   |  +---|-------+---+---+  
        |    | +--+    |   |                               +---+   |  |   +---+   +NodoNet|
        +    | |       |   |                               |     +-+--++      |   +-+---+-+ 
     +--+--+-+ | +--+--+   |                               |     |NodoC+--+   |     |   |
     |NodoNet+-+-+NodoA+   |                               |     +--+--+  |   | +---+   |
  +--+----+--+ | +---+-+   |                               |        |   +-+   | |       |
  |       |    |    |      |                               |        |   |  +--+-++      |
  |       +----+-+  |      |                               |        +-+ +--+NodoF|      |
 ++----+       | |  | +----+                               |          |    +-+---+      |
 |NodoB+-------+ |  + |                                    +-----+    |      |          |
 +--+--+         |  | |                                          |    |   +--+          |
    |            |  | |                                          |    |   |             |
    |          +-+--+-++                                         |  +-+---+-+-----------+
    +----------+NodoDLA|                                         +--+NodoDLA|   
               +--+----+     +--------+            +--------+       +--+----+
                  |          |   LED  |----Luz---->|Capturar|          |
                  +-------+->+--------+            +--------+->+-------+
                  |Arduino|                                    |Arduino|
                  +-------+<-+--------+            +--------+<-+--------
                             |Capturar|<---Luz-----|   LED  |      
                             +--------+            +--------+  



RFC  							IP - Standard									Fecha
Lisa							127.0								   Noviembre 2019
---------------------------------------------------------------------------- [page 3]



3. Requerimientos

+ La red abarca los siguiente requerimientos
+ Enviar mensajes entre los nodos de la subred
+ Los mensajes pueden viajar encryptados, si el usuario lo desea
+ El usuario puede consultar la tabla de routeo.
+ El usuario puede desconectar su nodo, cuando lo desee.
+ Pueden conectarse un máximo de 98 nodos por red, en cualquier momento
  mientras el server este esperando solicitudes.
+ Si se desea enviar un mensaje fuera de nuestra red Lisa, se deberá enrutar
  un nodo de salida como destino y así, el nodo saldrá hacia el servidor IRC.
+ Si desea enviar mensajes entre subredes debera escribir como enlaze entre
  las 2 redes el DLA vinculado a la subred actual


4. Terminología

+ Servidor:  Nodo especial que se encarga de escuchar a todos los nodos de
  la red, bajo el estándar de solicitud y respuesta
+ Nodo:  Dispositivo conectado o intentando conectar a la red Lisa
+ Camino:  Circuito de nodos a seguir hasta llegar a su camino
+ DLA:  Nodo  especial  de  luz,  el  cual  constara  solo  de  recepción  de  infor-
  mación y transferencia hacia un nodo idéntico
+ Clearnet:  Internet normal, fuera completamente de la red Lisa
+ Nodo Net:  Nodo salida hacia el clearnet



RFC  							IP - Standard									Fecha
Lisa							127.0								   Noviembre 2019
---------------------------------------------------------------------------- [page 4]



5. Operación General

La replicación de servidores, este procedimiento ocurre con la inserción de un
Nodo en la red Lisa, se encarga de enviar el usuario a al servidor A (A) que fue
registrado en el servidor B (B)

Registrar ------------------------------------------- A
A-----------------------------------------------------B

El usuario (U) realiza una solicitud al servidor (S), esta será directa.

solicitud ------------------------------------------->
U----------------------------------------------------S
<--------------------------------------------respuesta

Para  enviar  un  mensaje  a  un  nodo  que  se  encuentra  en  la  misma  ciudad,
este se puede enviar sin intermediarios, es decir sin pasar por otros nodos esto si
existe una conexión directa, en otro caso el mensaje pasara por cada uno de los
nodos hasta llegar al nodo destino.  Pueden existir mas de una ruta para llegar
al nodo destino.

mensaje -------------------------------------------->
A-----------B-------------C-------------D-----------E
<---------------------------------------------mensaje

Si se desea enviar un mensaje a un nodo que esta en otra ciudad, este debe
pasar sin excepción por el nodo DLA, este nodo es el que se encarga de comu-
nicar a las ciudades a través del medio físico.

mensaje-------------------------------------------->
A------------B-----------DLA-----------D-----------E
<--------------------------------------------mensaje

Si se desea enviar un mensaje a la red abierta,  este debe pasar por alg ́un
nodo  de  salida,  este  nodo  cambia  su  forma  de  interactuar  con  el  usuario,  ya
que no le dice nada sino que env ́ıa su mensaje a un servidor IRC previamente
instalado.

mensaje -------------------------------------------->
A-----------B-------------C-------------D-----------E
NET <----------IRC----------Clearnet----------mensaje 



RFC  							IP - Standard									Fecha
Lisa							127.0								   Noviembre 2019
---------------------------------------------------------------------------- [page 5]


6. Identificadores generales de nodos

6.1 ID

Cada nodo tendrá un identificador, estos se manejaran por letras del abecedario.
Así  que  cada  nodo  tendrá  asignada  una  letra  como  identificador,  la  red  no
sera muy grande, por este motivo se utiliza el abecedario para la asignación de
identificadores.  El nodo que comunicara las ciudades mediante el medio físico
tiene como nombre DLA. Y el nodo de salida ira bajo el nombre de NET.

6.2 Camino

Se puede seleccionar un camino al digitar el nombre de los nodos por los cuales
se desea que viaje el mensaje, separados por el signo (,) por ejemplo
A, B, C, D.

7. Capa Física

El medio físico que utiliza esta capa es la luz.  Esta capa es administrada por
un arduino, que funciona como una interfaz de red.  Esta interfaz de red ser ́a
la encargada de comunicar las ciudades que existan en la red lisa.  Cada ciudad
contara con un nodo encargado de enviar y recibir paquetes.  Los nodos envían
por medio de un led y reciben por una foto-resistencia, el mensaje es enviado
bit por bit donde encendido significa 1 y apagado significa 0.


8. Solicitudes al servidor

Todos  los  nodos  que  existan  en  la  red  Lisa,  incluso  el  nodo  que  sirve  como
puerta de enlace entre ciudades, mantendr ́an una comunicación constante con
el servidor, cada vez que un nuevo nodo se conecte al servidor este notificara a
todos los nodos existentes de la nueva conexión.
Cada uno de los nodos tiene comunicaci ́on directa con el servidor y puede realizar
solicitudes como ”registrar” que registra al nodo en la tabla de ruteo, cuando
este se conecta, solicitar los ”nodos” existentes en lisa o salir de la red.

9. Identificar nodos

Por id se identifican los nodos en la tabla de ruteo


RFC  							IP - Standard									Fecha
Lisa							127.0								   Noviembre 2019
---------------------------------------------------------------------------- [page 6]

10. Conexiones entre nodos

La conexión entre nodos es dinámica,  es decir cada nodo puede conectarse al
nodo que desee en su misma sub-red dentro de la red Lisa, ya sea para establecer
una comunicación o para servir como puente durante el env ́ıo de paquetes.

11. Desiciones dentro de los nodos

Conocen la tabla de routeo envió de paquetes recepción de paquetes

12. Redirección entre nodos

Cuando  un  paquete  esta  viajando  por  la  red  Lisa,  este  pasara  por  distintos
nodos por lo que el re-direccionamiento es muy importante.  Si el nodo al que
viaja el paquete esta en otra ciudad este deberá ser enviando por el nodo(DLA)
puerta de enlace, por otro lado si el nodo si esta en la ciudad entonces se envía
al siguiente nodo que indique la ruta brindada por el usuario.

13. Funcionamiento del nodo DLA

Este nodo funciona como puerta de enlace entre ciudades y hace uso de la capa
física(Luz), este nodo puede enviar paquetes a un nodo en la red Lisa o a otro
nodo que funcione como puerta de enlace.  Este nodo env ́ıa el paquete que recibe
junto al camino que debe seguir al otro nodo (puerta de enlace) una vez llegue
a la otra ciudad, al recibir, este toma el paquete y lo env ́ıa al siguiente nodo que
indica el camino y el proceso continua dentro de la red Lisa en la otra ciudad
hasta que el paquete llegue a su destino.



RFC  							IP - Standard									Fecha
Lisa							127.0								   Noviembre 2019
---------------------------------------------------------------------------- [page 7]


14. Paso de información

14.1 Nodo a Server

14.1.1 Solicitudes

Esta interacción es por solicitudes, el nodo envía su solicitud y luego el server
toma las decisiones

+ ”Registrar-Lobezna”
+ ”Registrar-Raptor”
+ ”Registrar fisico Raptor”
+ ”Registrar fisico Lobezna”
+ ”Registrar net Raptor”
+ ”Registrar net Lobezna”
+ ”nodos”
+ ”dc”
+ ”encriptar”+camino+delimitador+
+ ”replicar

14.2 Servidor a Servidor


14.2.1 Replicar

Esta solicitud lo que hace es añadir un nodo extra a la tabla pero de la subred
vecina.

14.3 Servidor a nodo

14.3.1 Registrar

Dependiendo  de  que  tipo  la  respuesta  varia,  pero  en  síntesis  es  la  misma:
id,ip,port,llave

+ Header :  Ninguno
+ Delimitador :  ”,”
+ Mensaje :  id+ip+port+llave


RFC  							IP - Standard									Fecha
Lisa							127.0								   Noviembre 2019
---------------------------------------------------------------------------- [page 8]


14.3.2 Nodos

+ Header :  1
+ Delimitador :  ”,” entre nodos y ”—” entre elementos internos del nodo
+ Mensaje :  Todos los nodos de la tabla de routeo

14.3.3 dc

Esta solicitud no tiene ningun mensaje, solo desconecta al nodo de la red

14.4.4  ”encriptar”+camino+delimitador+mensaje

Esta solicitud, es para encriptar el mensaje.
+ Header :  Camino
+ Delimitador :  ”—-”
+ Mensaje :  id+mensaje

14.5 Nodo a Nodo

El envío de paquetes entre nodos (DLA) se hace por medio de un led y se recibe
por una foto-resistencia.  El paso de información entre los nodos en Lisa se hace
a través de paquetes usando Scapy.  Esta comunicación solo tiene un propósito,
no hay respuesta.

14.5.1 Mensaje entre la misma red

+ Header :  Camino a seguir del nodo formato - ”nodo,nodo,destino”
+ Delimitador :  Secuencia de caracter ”—-”
+ Mensaje :  Esta divido en su primer byte reside el id del source y de ah ́ı
hasta que se acaben, radica el mensaje

14.6 Nodo Lisa a Nodo DLA y viceversa

Cuando un paquete va ser enviado de un nodo Lisa a un nodo DLA, el nodo
Lisa  hace  uso  de  la  biblioteca  DispositivoLuzAdaptador  y  utiliza  la  función
”enviar mensaje(paquete)” esta se encarga de enviar el paquete al nodo DLA
haciendo uso del medio físico.  Ahora si se desea enviar del nodo DLA al nodo
Lisa, una vez que un nodo DLA recibe un paquete y debe enviarlo a un nodo
Lisa este busca en el paquete el camino dado, selecciona el nodo siguiente y lo
envía.
La  ́unica condición que se debe cumplir antes de enviar un paquete de un nodo
lisa a un nodo DLA es que el paquete debe ser transformado a tipo string.


RFC  							IP - Standard									Fecha
Lisa							127.0								   Noviembre 2019
---------------------------------------------------------------------------- [page 9]


14.7 Nodo DLA a Nodo DLA

+ Header :  Tamaño completo y Checksum del mensaje
+ Delimitador :  1 entre cada trama
+ Trama:  Tamaño 300 bits
+ Mensaje :  mensaje binario

14.8 Nodo Lisa a Nodo Clearnet

Cualquier nodo lisa puede comunicarse con el nodo clearnet, quien es el punto
de salida a Clearnet.  El envío de paquetes entre un nodo lisa y un nodo Clearnet
ocurre de la misma forma en la que se pasa información de nodo a nodo lisa.  Los
nodos lisa solo deben enviar su mensaje al nodo clearnet y este se encarga de
lanzarlo a Clearnet.  El usuario es quien toma la decisi ́on de enviar su mensaje
a clearnet, si desea lanzarlo debe seleccionar el nodo clearnet como destino

